dnl
dnl There aren't any macro expansions; it's just an example.
dnl

class hello
{
public:
  void
  say (char const* phrase);
};
