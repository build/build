# file      : examples/cxx/hello/libhello/build/bootstrap.make
# copyright : Copyright (c) 2004-2012 Code Synthesis Tools CC
# license   : GNU GPL v2; see accompanying LICENSE file

project_name := libhello

include $(dir $(lastword $(MAKEFILE_LIST)))../../../../../build/bootstrap.make
