# file      : examples/cxx/hello/hello/build/bootstrap.make
# copyright : Copyright (c) 2004-2012 Code Synthesis Tools CC
# license   : GNU GPL v2; see accompanying LICENSE file

project_name := hello driver

include $(dir $(lastword $(MAKEFILE_LIST)))../../../../../build/bootstrap.make
